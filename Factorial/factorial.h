//**********************************************************************
//  Fecha: 01-10-2019
//  Nombre: factorial.h
//  Objetivo: Mostrar el resultado de calcular el factorial de un número,
//            usando recursividad.
//  Tipo: Módulo
//  Plataforma: GNU/Linux
//  Colaboraciones: - Franyer Hidalgo VE - MITIS2
//**********************************************************************

#include <iostream>
#include <stdlib.h>

using namespace std;

// Función que retorna el resultado de el factorial de un número, de tipo entero largo
long int Factorial(int Num) // Toma como parámetro un número entero
{
   if(Num == 0) // Si el número ingresado es 0, retornamos 1 Caso base
   {
     return 1;
   }
   else // Si el número ingresado es diferente a 1, retornamos: 
   {
     return(Num * Factorial(Num - 1)); // La multiplicación del factorial del número base menos 1, se aplica la recursividad
     // La operación aritmética no se cumple hasta que la función factorial() haya resuelto su problema base  
   }
}

// Procedimiento que sirve como menu principal para llamar a la funcion factorial 
void menu_principal() 
{
  int Num; // Almacena el numero factorial a calcular 
  char res; // Almacena la respuesta del usuario para continuar o no con el programa
	cout << "**********************************************" << endl;
	cout << "\tPROGRAMA QUE CALCULA EL FACTORIAL"  << endl;
	cout << "\t\tDE UN NÚMERO"                     << endl;
	cout << "**********************************************" << endl;
	cout << "Ingresa un numero para calcular su factorial: ";
	cin >> Num;
	cout << "El factorial de " <<  Num << " es " << Factorial(Num) << endl; // Llama a la funcion factorial para mostrar el resultado del usuario 
	cout << endl << "¿Desea volver a calcular un número factorial? Afirmativo (Y/y), de lo contrario pulse cualquier tecla para salir: "; 
	cin >> res;
	if(res == 'y' or res == 'Y') // La negación de esta condición es el caso base
	{
		menu_principal();
	}
	else
	{
		cout << "Hasta luego, vuelva pronto.";
	} 
}

