//**********************************************************************
//  Fecha: 01-10-2019
//  Nombre: programa_principal.cpp
//  Objetivo: Funcion principal que hará llamado a las funciones o 
//            procedimientos contenidos en otros modulos.
//  Tipo: Principal
//  Plataforma: GNU/Linux
//  Colaboraciones: - Franyer Hidalgo VE - MITIS2
//**********************************************************************

#include "factorial.h" // Incluyendo al modulo que contiene las funciones para calcular el factorial de un número.
#include <iostream>
#include <stdlib.h>

using namespace std;

/*********************************************
APLICANDO EL PARADIGMA DE PROGRAMACIÓN MODULAR
**********************************************/
int main()
{
	menu_principal();
}
