//**********************************************************************
//  Fecha: 01-10-2019
//  Nombre: programa_principal.cpp
//  Objetivo: Funcion principal que hará llamado a las funciones o 
//            procedimientos contenidos en otros modulos.
//  Tipo: Principal
//  Plataforma: GNU/Linux
//  Colaboraciones: - Franyer Hidalgo VE - MITIS2
//**********************************************************************

#include<iostream>
#include"matriz.h"
using namespace std;

/*********************************************
APLICANDO EL PARADIGMA DE PROGRAMACIÓN MODULAR
**********************************************/
int main()
{
	int matriz_uno[2][2], matriz_dos[2][2], resultado[2][2];
	cout << "Llena la primera matriz\n";
	llenar_matriz(matriz_uno);
	cout << "\nLlena la segunda matriz\n";
	llenar_matriz(matriz_dos);
	cout << "\nSuma las 2 matrices\n";
	sumar(matriz_uno, matriz_dos, resultado);
	cout << "\nResultados\n";
	imprimir(resultado);
}

