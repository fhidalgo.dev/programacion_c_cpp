//**********************************************************************
//  Fecha: 31-01-2020
//  Nombre: matriz.h
//  Objetivo: Módulo que construye las funciones básicas para llenar 2
//            matrices, sumar sus valores y mostrar su resultados. Es una
//            matriz bidimensional 2x2.
//  Tipo: Principal
//  Plataforma: GNU/Linux
//  Colaboraciones: - Franyer Hidalgo VE - MITIS2
//**********************************************************************

#include <iostream>
using namespace std;

int llenar_matriz(int matriz[2][2])
{
	// Aqui se recorre la primera dimension de la matriz
	for(int i = 0; i < 2; i++)
	{
		// Aqui se recorre la segunda dimension de la matriz
		for(int j = 0; j < 2; j++)
		{
			// Se llena la primera matriz y despues la segunda 
			cout << "Ingresa el valor de la posición " << i + 1 << "-" << j + 1 << ": ";
			cin >> matriz[i][j];
		} 
	}

}


int sumar(int matriz_uno[2][2], int matriz_dos[2][2], int resultado[2][2])
{
	for(int i = 0; i < 2; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			// Se suman las 2 matrices con su respectiva posición
			resultado[i][j] = matriz_uno[i][j] + matriz_dos[i][j];
		}
	}
}

int imprimir(int salida[2][2])
{
	// Se imprimen primero los valores de la primera dimension y luego los de la segunda
	for(int i = 0; i < 2; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			cout << "\n";
			cout << "El resultado en la posición " << i + 1 << " " << j + 1 << " es: ";
			cout << salida[i][j];
		}
	}
}


