//**********************************************************************
//  Fecha: 15-10-2019
//  Nombre: EstructuraDinamica.h
//  Objetivo: Definir las funciones principales para reservar y liberar
//            memoria dinámmicamente utilizando listas simplemente enlazadas,
//            es decir, solo con un enlace al siguiente nodo.
//  Tipo: Módulo
//  Plataforma: GNU/Linux
//  Colaboraciones: - Grupo PNF-I 302-13
//                  - Franyer Hidalgo VE - MITIS2
//**********************************************************************


#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

struct Nodo{ // SE CREA UN REGISTRO QUE SERÁ EL NODO QUE CONTIENE EL DATO Y EL APUNTADOR A SIGUIENTE
    int cedula; // A EJEMPLO, DECLARAMOS UNA VARIABLE TIPO ENTERO LLADAMA cedula, QUE ALMACENARA "teoricamente" NUMEROS DE CEDULAS
    Nodo *siguiente;
};
Nodo *cabeza = NULL; // APUNTAR A LA CABEZA DE LA LISTA TIPO GLOBAL POR SU CONSTANTE USO EN FUNCIONES
Nodo *fin; // APUNTAR Al FINAL DE LA LISTA TIPO GLOBAL POR SU CONSTANTE USO EN FUNCIONES


//////////////////////INSERTAR AL INICIO///////////////////////////////
void insertarInicio()
{
    Nodo* nuevo = new Nodo(); // CREAR UN NUEVO NODO, LLAMANDO A LA ESTRUCTURA YA CREADA
	// ESE NUEVO NODO ES LLAMADO CON LA VARIABLE nuevo
    cout << " Ingrese el numero de cedula que contendra el nuevo Nodo: ";
    cin >> nuevo->cedula; // nuevo, ES UN REGISTRO DE NODO. AQUI SE APUNTA AL DATO QUE HAY DENTRO DEL REGISTRO.
    
    if(cabeza == NULL){ // SI LA CABEZA(primer dato) DE LA LISTA ES NULO
        cabeza = nuevo; // A LA CABEZA DE NUESTRA LISTA VA A SER IGUAL AL NUEVO DATO
        cabeza->siguiente = NULL; // DECLARAR EL siguiente DE NUESTRA LISTA COMO NULO
        fin = nuevo; // ASIGNAR EL NODO nuevo AL FINAL DE LA LISTA
        cout << "\nPrimer Dato\n"; 
	}
	else{
		nuevo->siguiente = cabeza;
        cabeza = nuevo; // A LA CABEZA DE NUESTRA LISTA VA A SER IGUAL AL NUEVO DATO
	}
}
 
//////////////////////INSERTAR AL FINAL///////////////////////////////
void insertarFinal()
{
    Nodo* nuevo = new Nodo(); // CREAR UN NUEVO NODO, LLAMANDO A LA ESTRUCTURA YA CREADA
	// ESE NUEVO NODO ES LLAMADO CON LA VARIABLE nuevo
    cout << " Ingrese el numero de cedula que contendra el nuevo Nodo: ";
    cin >> nuevo->cedula; // nuevo, ES UN REGISTRO DE NODO. AQUI SE APUNTA AL DATO QUE HAY DENTRO DEL REGISTRO.
    
    if(cabeza == NULL){ // SI LA CABEZA(primer dato) DE LA LISTA ES NULO
        cabeza = nuevo; // A LA CABEZA DE NUESTRA LISTA VA A SER IGUAL AL NUEVO DATO
        cabeza->siguiente = NULL; // DECLARAR EL siguiente DE NUESTRA LISTA COMO NULO
        fin = nuevo; // ASIGNAR EL NODO nuevo AL FINAL DE LA LISTA
        cout << "\nPrimer Dato\n";  
    }
    else{ // SI LA cabeza DEL NODO NO ES NULA
        fin->siguiente = nuevo; // INGRESAR EL NUEVO NODO AL siguiente DEL NODO FINAL DE LA LISTA
        nuevo->siguiente = NULL; // DECLARAR EL siguiente de NUESTRA LISTA COMO NULO
        fin = nuevo; // ASIGNAR EL NUEVO NODO nuevo AL FINAL DE LA LISTA 
    }
    cout << "\n Nodo Ingresado Correctamente en la Lista\n\n";
}

//////////////////////INSERTAR ANTES O DESPUES DE DETERMINADA POSICIÓN///////////////////////////////
int insertarAntesDespues(int pos)
{
    int _op, band;
    cout << endl;
    cout << "\t Donde quiere insertar la nueva cedula" << endl;
    cout << "\t 1. Antes de la posicion " << pos << endl;
    cout << "\t 2. Despues de la posicion " << pos << endl;
 
    cout << "\n\t Opcion : "; 
    cin >> _op;
 
    if(_op == 1)
        band = -1;
    else
        band = 0;
 
    return band;
}

//////////////////////INSERTAR ELEMENTO EN UNA DETERMINADA POSICIÓN///////////////////////////////
void insertarElemento()
{
    Nodo* nuevo = new Nodo(); // CREAR UN NUEVO NODO, LLAMANDO A LA ESTRUCTURA YA CREADA
    Nodo* aux; // NODO AUXILIAR PARA RECORRER LOS NODOS
	// ESE NUEVO NODO ES LLAMADO CON LA VARIABLE nuevo
    cout << "Ingrese el numero de cedula que contendra el nuevo Nodo: ";
    cin >> nuevo->cedula; // nuevo, ES UN REGISTRO DE NODO. AQUI SE APUNTA AL DATO QUE HAY DENTRO DEL REGISTRO.
	int pos;
	cout << "\nIngrese la posición en la que se insertara el nuevo nodo: ";
	cin >> pos;
	
    if(pos == 1)
    {
		nuevo->siguiente = cabeza;
        cabeza = nuevo; // A LA CABEZA DE NUESTRA LISTA VA A SER IGUAL AL NUEVO DATO
    }
    else
    {
        int x = insertarAntesDespues(pos);
        aux = cabeza;
 
        for(int i = 1; aux != NULL; i++)
        {
            if(i == pos + x){
                nuevo->siguiente = aux->siguiente;
                aux->siguiente = nuevo;
                return;
            }
            aux = aux->siguiente;
        }
    }
    cout<<"   Error...Posicion no encontrada..!"<<endl;
}

//////////////////MOSTRAR TODOS LOS ELEMENTOS////////////////////////
void desplegarLista(){
    int i = 0; // Contador
    Nodo *actual = new Nodo(); // CREAR UN NODO, ESTE NODOS actual RECORRERA LAS CEDULAS DE LISTA 
    actual = cabeza; // actual EMPEZARA RECORRIENDO DESDE LA cabeza DE LA LISTA
    if(cabeza != NULL){ // CONDICION PARA VERIFICAR QUE LA LISTA TENGA VALORES
        cout  << "\nLas cedulas registradas son las siguientes\n\n";
        while(actual != NULL){
            cout << i+1 << "--> " << actual->cedula  << endl; // MUESTRO LA CEDULA DEL NODO ACTUAL
            actual = actual->siguiente; // EN CADA RECORRIDO ACTUALIZO EL NODO actual
            i++;
        }
        
    }
    else{
        cout  << "\nLa Lista no tiene cedulas registradas\n\n";
    }
}

//////////////////ELIMINACIÓN////////////////////////
void eliminarNodo(){
    Nodo* actual = new Nodo(); // Nodo actual QUE RECORRERA TODOS LOS NODOS
    actual = cabeza;
    Nodo* anterior = new Nodo(); // NODO QUE GUARDARÁ EL NODO ACTUAL RECORRIDO
    anterior = NULL;
    Nodo* borrar; // NODO APUNTADOR QUE GUARDA LA DIRECCION DE MEMORIA A LIBERAR
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << " Ingrese la cedula del nodo a Buscar para desenlazar: ";
    cin >> nodoBuscado;
    if(cabeza != NULL){

        while(actual != NULL && encontrado != true){
            
            if(actual->cedula == nodoBuscado){
                cout << "\n Nodo con el dato " << nodoBuscado << " Encontrado\n\n";
                
                if(actual == cabeza){ // CASO SI SE ELIMINA LA PRIMERA cedula DE LA LISTA
                    borrar = cabeza; 
                    cabeza = cabeza->siguiente;
                    delete(borrar); // MEMORIA LIBERADA
                }
                else if(actual == fin){ // CASO SI SE ELIMINA LA ULTIMA cedula DE LA LISTA
                    borrar = fin;
                    anterior->siguiente = NULL;
                    fin = anterior;
                    delete(borrar); // MEMORIA LIBERADA
                }
				else{ // CASO SI SE ELIMINA CUALQUIER cedula QUE NO SEA LA ULTIMA NI LA PRIMERA
					borrar = actual;
					anterior->siguiente = actual->siguiente; // ENLAZO EL NODO anterior CON EL NODO SIGUIENTE DEL actual REGISTRO
					delete(borrar); // MEMORIA LIBERADA
				}
                
                cout << "\n Cedula desenlazada con exito y memoria liberada\n\n";
                
                encontrado = true;
            }
            
            anterior = actual;
            actual = actual->siguiente;
        }
        if(!encontrado){
            cout << "\nCedula No Encontrada en el nodo\n\n";
        }
    }
    else{
        cout  << "\n La Lista se Encuentra Vacia\n\n";
    }
}
