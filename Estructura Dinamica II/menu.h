//**********************************************************************
//  Fecha: 15-10-2019
//  Nombre: menu.h
//  Objetivo: Mostrar menú al usuario haciendo llamado a las funciones
//            y procedimientos declarados en el archivo EstructuraDinamica.h
//  Tipo: Módulo
//  Plataforma: GNU/Linux
//  Colaboraciones: - Grupo PNF-I 302-13
//                  - Franyer Hidalgo VE - MITIS2
//**********************************************************************

#include <iostream>
#include "EstructuraDinamica.h"
#include <stdio.h>

//////////////////////MOSTRAR OPCIONES DEL MENU///////////////////////////////
void OpcionesMenu()
{
    cout << "*****************************\n";
    cout << "LISTAS DOBLEMENTE ENLAZADAS\n";
    cout << "*****************************\n";
    cout << "\n\t\tESTRUCTURA DINAMICA\n\n";
    cout << " 1. INSERTAR CEDULA                  " << endl;
    cout << " 2. MOSTRAR CEDULA                   " << endl;
    cout << " 3. ELIMINAR CEDULA                  " << endl;
    cout << " 4. SALIR                            " << endl;

    cout<<"\n INGRESE OPCION: ";
}

//////////////////////MOSTRAR MENU///////////////////////////////
void menu(void)
{
    int op;  // OPCION DEL MENU
    bool band = false;

    do
    {
        OpcionesMenu();
        cin >> op;

        switch(op) // SWITCH PARA SELECCIONAR LAS OPCIONES CORRESPONDIENTES
        {
            case 1:
				cin.ignore();
                insertarNodo();
                break;
            case 2:
				cin.ignore();
                desplegarLista();
                break;             
            case 3:
				cin.ignore();
                eliminarNodo();
                break;
            case 4:
				band = true;
				exit(1);               

            default:
				cout << "OPCION NO VALIDA...!!! \nPulse cualquier tecla para continuar ...";
				cin.ignore();
				cin.get();
                break;
        }
        cout << endl << endl;
    }while(band == false);

}
