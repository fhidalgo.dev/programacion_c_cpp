//**********************************************************************
//  Fecha: 15-10-2019
//  Nombre: EstructuraDinamica.h
//  Objetivo: Definir las funciones principales para reservar y liberar
//            memoria dinámmicamente utilizando listas doblemente enlazadas.
//  Tipo: Módulo
//  Plataforma: GNU/Linux
//  Colaboraciones: - Grupo PNF-I 302-13
//                  - Franyer Hidalgo VE - MITIS2
//**********************************************************************

#include <iostream>
#include <cstdlib>
#include <stdio.h>

using namespace std;

struct Nodo{ // SE CREA UN REGISTRO QUE SERÁ EL NODO QUE CONTIENE 2 DATOS, EL APUNTADOR AL SIGUIENTE Y ANTERIOR NODO
    int cedula; // A EJEMPLO, DECLARAMOS UNA VARIABLE TIPO ENTERO LLADAMA cedula, QUE ALMACENARA "teoricamente" NUMEROS DE CEDULAS
    string nombre; // A EJEMPLO, DECLARAMOS UNA VARIABLE TIPO CHAR LLADAMA nombre, QUE ALMACENARA "teoricamente" NOMBRES DE PERSONAS
    Nodo *siguiente;
    Nodo *anterior;
};
Nodo *cabeza = NULL; // APUNTAR A LA CABEZA DE LA LISTA TIPO GLOBAL POR SU CONSTANTE USO EN FUNCIONES
Nodo *fin; // APUNTAR Al FINAL DE LA LISTA TIPO GLOBAL POR SU CONSTANTE USO EN FUNCIONES


//////////////////////INSERTAR ELEMENTO///////////////////////////////
void insertarNodo(){
    Nodo* nuevo = new Nodo(); // CREAR UN NUEVO NODO, LLAMANDO A LA ESTRUCTURA YA CREADA
	// ESE NUEVO NODO ES LLAMADO CON LA VARIABLE nuevo
    cout << "\nNombre: ";
    cin >> nuevo->nombre; // nuevo, ES UN REGISTRO DE NODO. AQUI SE APUNTA AL DATO QUE HAY DENTRO DEL REGISTRO.
    cout << "\nCedula: ";
    cin >> nuevo->cedula; // nuevo, ES UN REGISTRO DE NODO. AQUI SE APUNTA AL DATO QUE HAY DENTRO DEL REGISTRO.
    
    if(cabeza == NULL){ // SI LA CABEZA(primer dato) DE LA LISTA ES NULO
        cabeza = nuevo; // A LA CABEZA DE NUESTRA LISTA VA A SER IGUAL AL NUEVO DATO
        cabeza->siguiente = NULL; // DECLARAR EL siguiente DE NUESTRA LISTA COMO NULO
        cabeza->anterior = NULL; // DECLARAR EL anterior DE NUESTRA LISTA COMO NULO
        fin = nuevo; // ASIGNAR EL NODO nuevo AL FINAL DE LA LISTA 
    }
    else{ // SI LA cabeza DEL NODO NO ES NULA
        fin->siguiente = nuevo; // INGRESAR EL NUEVO NODO AL siguiente DEL NODO FINAL DE LA LISTA
        nuevo->siguiente = NULL; // DECLARAR EL siguiente de NUESTRA LISTA COMO NULO
        nuevo->anterior = fin; // ASIGNAR EL anterior de NUESTRA LISTA AL ULTIMO NODO ACTUAL
        fin = nuevo; // ASIGNAR EL NUEVO NODO nuevo AL FINAL DE LA LISTA 
    }
    cout << "\n Nodo Ingresado Correctamente en la Lista\n\n";
}  

//////////////////MOSTRAR TODOS LOS ELEMENTOS////////////////////////
void desplegarLista(){
    Nodo *actual; // ESTE NODO actual RECORRERA LAS CEDULAS DE LISTA 
    actual = cabeza; // actual EMPEZARA RECORRIENDO DESDE LA cabeza DE LA LISTA
    if(cabeza != NULL){ // CONDICION PARA VERIFICAR QUE LA LISTA TENGA VALORES
        cout  << "\nLas personas registradas son las siguientes\n\n";
        cout  << "Nombre: Cedula" << endl;
        while(actual != NULL){
            cout << "--> " << actual->nombre << ": " << actual->cedula  << endl; // MUESTRO EL NOMBRE Y LA CEDULA DEL NODO ACTUAL
            actual = actual->siguiente; // EN CADA RECORRIDO ACTUALIZO EL NODO actual
        }
        
    }
    else{
        cout  << "\nLa Lista no tiene registros\n\n";
    }
}

//////////////////ELIMINACIÓN////////////////////////
void eliminarNodo(){
    Nodo* actual; // Nodo actual QUE RECORRERA TODOS LOS NODOS
    Nodo* aux; // Nodo actual QUE RECORRERA TODOS LOS NODOS
    aux = NULL;
    actual = cabeza;
    Nodo* anterior = new Nodo(); // NODO QUE GUARDARÁ EL NODO ACTUAL RECORRIDO
    anterior = NULL;
    Nodo* borrar; // NODO APUNTADOR QUE GUARDA LA DIRECCION DE MEMORIA A LIBERAR
    bool encontrado = false;
    int nodoBuscado = 0;
    cout << " Ingrese la cedula del nodo a Buscar para desenlazar: ";
    cin >> nodoBuscado;
    if(cabeza != NULL){

        while(actual != NULL && encontrado != true){
            
            if(actual->cedula == nodoBuscado){
                cout << "\n Nodo con el dato " << nodoBuscado << " Encontrado\n\n";
                
                if(actual == cabeza){ // CASO SI SE ELIMINA LA PRIMERA cedula DE LA LISTA
                    borrar = cabeza; 
                    cabeza = cabeza->siguiente;
                    cabeza->anterior = NULL;
                    delete(borrar); // MEMORIA LIBERADA
                }
                else if(actual == fin){ // CASO SI SE ELIMINA LA ULTIMA cedula DE LA LISTA
                    borrar = fin;
					anterior->siguiente = NULL;
                    fin = anterior;
                    delete(borrar); // MEMORIA LIBERADA
                }
				else{ // CASO SI SE ELIMINA CUALQUIER cedula QUE NO SEA LA ULTIMA NI LA PRIMERA
					borrar = actual;
					anterior->siguiente = actual->siguiente; // ENLAZO EL NODO anterior CON EL NODO SIGUIENTE DEL actual REGISTRO
					
					aux = actual->siguiente; // ENLAZO EL NODO anterior CON EL NODO SIGUIENTE DEL actual REGISTRO
					aux->anterior = anterior;
					delete(borrar); // MEMORIA LIBERADA
				}
                
                cout << "\n Cedula desenlazada con exito y memoria liberada\n\n";
                
                encontrado = true;
            }
            
            anterior = actual;
            actual = actual->siguiente;
        }
        if(!encontrado){
            cout << "\nCedula No Encontrada en el nodo\n\n";
        }
    }
    else{
        cout  << "\n La Lista se Encuentra Vacia\n\n";
    }
}

