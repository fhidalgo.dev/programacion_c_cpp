//**********************************************************************
//  Fecha: 15-10-2019
//  Nombre: programa_principal.h
//  Objetivo: Hacer llamado a las funciones pertinentes para su ejecución
//            en la función principal.
//  Tipo: Módulo
//  Plataforma: GNU/Linux
//  Colaboraciones: - Grupo PNF-I 302-13
//                  - Franyer Hidalgo VE - MITIS2
//**********************************************************************

#include <iostream>
#include "menu.h"
#include <stdio.h>

using namespace std;

/*********************************************
APLICANDO EL PARADIGMA DE PROGRAMACIÓN MODULAR
**********************************************/
int main(){
    menu();
    return 0;
}
